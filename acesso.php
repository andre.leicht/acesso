<?php
		session_start();
		if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
		{
		  unset($_SESSION['login']);
		  unset($_SESSION['senha']);?><script> location.replace('index.php'); </script>"<?php
		  }
		 
		$logado = $_SESSION['login'];
	?>
<!doctype html>
<html lang="pt-BR">
  <head>
  	
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="CSS/style.css">
    <script src="JavaScrip/scripts.js"></script>
    <title>..::Acesso::..</title>
  </head>
  <body>
  	<div class="container">
	  	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
		    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		      <li class="nav-item">
		        <a class="nav-link" href="inicio.php">Inicío</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="acesso.php">Acesso</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="usuarios.php">Usuários</a>
		      </li>		      
		    </ul>
		    <ul class="navbar-nav navbar-right">
		    	<li class="nav-item">
		      		<a class="nav-link mt-2 mt-lg-0" href="logoff.php" id="logoff" onclick="alerta();">Logoff</a>
		      	</li>	
		    </ul>
		  </div>
		</nav>
		<br/>

		<div class="row">
			<div class="col-md-3"></div>
			<form action="acesso.php" method="post" role="form" class="bg-secondary form_2 col-md-6">
				<div class="form-group">
					<div class="row">										
						<div class="col-md-2 margem_1">
							<label class="text-white" for="pesquisar_acesso">Placa (opcional)</label>
						</div>
						<div class="col-md-10"></div>
					</div>
					<div class="row" >
						<div class="col-md-2 margem_1">
							<input type="text" name="pesquisar_acesso" id="pesquisar_acesso" placeholder="AAA-1111" style="max-width:80px; margin-top: 8px;" maxlength="7" onkeyup="this.value = this.value.toUpperCase()"/>
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-success" name="entrar_acesso">Pequisar</button>
						</div>
						<div class="col-md-5"></div>
						<div class="col-md-3" >
							<button type="button" class="btn btn-primary" name="cadastrar_acesso" onclick="window.location.replace('cadastrar.php')">
								Cadastrar
							</button>
							<input type="hidden" name="exibir_tabela" value ="true"/>
						</div>
						
					</div>
				</div>
			</form>
			<div class="col-md-3"></div>
		</div>
		
		<div>
			<?php 
				if ((isset ($_POST['exibir_tabela']) == true) and ($_POST['exibir_tabela'] == "true")){

					include "db_class.php";
					$Obj_db = new db();
					$link = $Obj_db ->conecta_mysqli();
					if ((isset ($_POST['pesquisar_acesso']) == true) and ($_POST['pesquisar_acesso'] <> '')) {
						$par_placa = $_POST['pesquisar_acesso'];
					}else{
						$par_placa = '%';
					}	
					

					$sql =  "SELECT ".
						   	" ace.placa, ".
						   	" DATE_FORMAT( ace.data_ent , '%d/%m/%Y - %H:%i') as data_ent, ".
							" DATE_FORMAT( acs.data_sai,  '%d/%m/%Y - %H:%i') as data_sai, ".    	
						    " TIMESTAMPDIFF(minute, ace.data_ent, coalesce(acs.data_sai, DATE_ADD(now(), INTERVAL -3 HOUR))) as ".
						    " permanencia_minutos, ".
							" TIMESTAMPDIFF(day, ace.data_ent, coalesce(acs.data_sai, DATE_ADD(now(), INTERVAL -3 HOUR))) as permanencia_dias ".
						    " FROM acesso_entrada ace ".
							" INNER JOIN saldo_acesso sa on (ace.protocolo = sa.protocolo_ent) ".
							" LEFT JOIN acesso_saida acs on (sa.protocolo_sai = acs.protocolo) ".
							" WHERE ace.placa like '$par_placa' ";

					$resultado = mysqli_query($link,$sql);

			?>	
					<br/>
					<table class="table bg-secondary">
					  	<thead class="thead-dark">
						    <tr>
						        <th scope="col">Placa</th>
						      	<th scope="col">Data Entrada</th>
						      	<th scope="col">Data Saida</th>
						      	<th scope="col">Permanência(Minutos)</th>	
						    	<th scope="col">Permanência(Dias)</th>
						    </tr>
					  	</thead>
						<tbody>
							<?php 
							while($dados = mysqli_fetch_array($resultado)){
						  		$placa    = $dados['placa'];
							  	$data_ent = $dados['data_ent'];
							  	$data_sai = $dados['data_sai'];
							  	$minutos  = $dados['permanencia_minutos'];
							  	$dias 	  = $dados['permanencia_dias']; ?>
								<tr>
							      	<td><?php echo $placa ?></td>
							      	<td><?php echo $data_ent ?></td>
							      	<td><?php echo $data_sai ?></td>
							      	<td><?php echo $minutos ?></td>
							      	<td><?php echo $dias ?></td>
								</tr> 
							<?php } ?>
						</tbody>
					</table>	
				<?php } ?>		
		</div>
	</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>