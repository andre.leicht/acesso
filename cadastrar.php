<?php
			session_start();
			if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
			{
			  unset($_SESSION['login']);
			  unset($_SESSION['senha']);?>
	  <script> location.replace("index.php"); </script><?php
			  }
			 
			$logado = $_SESSION['login'];
		?>
<!doctype html>
<html lang="pt-BR">
  	<head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		
		<link rel="stylesheet" type="text/css" href="CSS/style.css">
		
	    
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	        	       
    	<title>..::Acesso::..</title>
    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
	    <link rel="stylesheet" type="text/css" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" />
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
		<script src="JavaScrip/scripts.js"></script>
    	
  	</head>
  	<body>
	  	<div class="container">
		  	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
			    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			      <li class="nav-item">
			        <a class="nav-link" href="inicio.php">Inicío</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" href="acesso.php">Acesso</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" href="usuarios.php">Usuários</a>
			      </li>
			      	      
			    </ul>
			    <ul class="navbar-nav navbar-right">
			    	<li class="nav-item">
			      		<a class="nav-link mt-2 mt-lg-0" href="logoff.php" id="logoff" onclick="alerta();">Logoff</a>
			      	</li>	
			    </ul>			    
			  </div>
			</nav>
			<br/>

			
			<form action="enviar_cadastro.php" method="post" role="form" class="bg-secondary form_3" style="">
				<div class="form-group">
					<div class="form-row">
		        		<div class=" col-md-4"></div>							
						<label for="inp_placa" class="control-label col-md-4 col-sm-4 col-xs-4">Placa</label>
					</div>
					<div class="form-row">
		        		<div class=" col-md-4"></div>
						<input type="text" name="inp_placa" id="inp_placa" 
						class="form-control col-md-4 col-sm-4 col-xs-4" maxlength="7"
						onkeyup="this.value = this.value.toUpperCase()" />
					</div>			
				</div>

				<div class="form-group" >
					<div class="form-row">
		        		<div class=" col-md-4"></div>
					</div>
					<div class="form-row">
		        		<div class=" col-md-4"></div>	
						<div class="col-md-4 col-sm-4 col-xs-4">

						</div>
					</div>
				</div>

		        <div class="form-group">
		        	<div class="form-row">
		        		<div class=" col-md-4"></div>
		        		<label for="inp_data" class="control-label col-md-4 col-sm-4 col-xs-4"> Data</label>
		        	</div>
		        	<div class="form-row">	        		
		        		<div class=" col-md-4"></div>				
	                  	<div class='input-group date col-md-4 col-sm-4 col-xs-4' id='datetimepicker5'>
							<input type='datetime-local' class="form-control" maxlength="16" style="width: 90%"
							id="inp_data" name="inp_data" />							
						</div>			               		                                
		        	</div>
	            </div>       	

	        	<div class="form-group">
	        		<div class="form-row">
	        			<div class=" col-md-4"></div>
						<button type="button" class="btn btn-danger col-md-2 col-sm-2 col-xs-2"  name="cancelar" 
						onclick="limpar_campos();">
							Cancelar
						</button>        				
			    			
						<button type="submit" class="btn btn-success col-md-2 col-sm-2 col-xs-2" name="entrar">
							Entrar
						</button>
					</div>
				</div>
			</form>
		</div>		
  	</body>
</html>