<?php
	class db {
		//host
		private $host = 'localhost';

		//usuario
		private $usuario = 'id10340660_admin';

		//senha
		private $senha = 'admin';

		//banco
		private $banco = 'id10340660_acesso_db';

		public function conecta_mysqli(){
			$con = mysqli_connect($this->host, $this->usuario, $this->senha, $this->banco);
			mysqli_set_charset($con, 'utf8');

			if(mysqli_connect_errno()){
				echo 'Erro'.mysqli_connect_error();
			}

			return $con;
		}
	}
?>