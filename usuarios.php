<?php
	session_start();
	if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
	{
	  unset($_SESSION['login']);
	  unset($_SESSION['senha']);?>
  		<script> location.replace("index.php"); </script><?php
	  }
	 
	$logado = $_SESSION['login'];
	?>
<!doctype html>
<html lang="pt-BR">
  <head>
  	
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="CSS/style.css">
    <script src="JavaScrip/scripts.js"></script>
    <title>..::Acesso::..</title>
  </head>
  <body>
  	<div class="container">
	  	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
		    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		      <li class="nav-item">
		        <a class="nav-link" href="inicio.php">Inicío</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="acesso.php">Acesso</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="usuarios.php">Usuários</a>
		      </li>	      
		    </ul>
		    <ul class="navbar-nav navbar-right">
		    	<li class="nav-item">
		      		<a class="nav-link mt-2 mt-lg-0" href="logoff.php" id="logoff" onclick="alerta();">Logoff</a>
		      	</li>	
		    </ul>
		  </div>
		</nav>
		
		<div>
			<?php 				

				include "db_class.php";
				$Obj_db = new db();
				$link = $Obj_db ->conecta_mysqli();
				
				$sql =  "SELECT nome, sobrenome, descricao_tipo, CPF, RG, email".						   
					    " FROM usuarios us ".
						" INNER JOIN tipo_usuario tu on (us.tipo_usuario = tu.id)";

				$resultado = mysqli_query($link,$sql);

			?>	
			<br/>
			<table class="table bg-secondary">
			  	<thead class="thead-dark">
				    <tr>
				        <th scope="col">Nome</th>
				      	<th scope="col">Sobrenome Entrada</th>
				      	<th scope="col">Função</th>
				      	<th scope="col">CPF</th>	
				    	<th scope="col">RG</th>
				    	<th scope="col">Email</th>
				    </tr>
			  	</thead>
				<tbody>
					<?php 
					while($dados = mysqli_fetch_array($resultado)){
				  		$nome    = $dados['nome'];
					  	$sobre = $dados['sobrenome'];
					  	$funcao = $dados['descricao_tipo'];
					  	$CPF  = $dados['CPF'];
					  	$RG 	  = $dados['RG']; 
					  	$email 	  = $dados['email'];?>
						<tr>
					      	<td><?php echo $nome ?></td>
					      	<td><?php echo $sobre ?></td>
					      	<td><?php echo $funcao ?></td>
					      	<td><?php echo $CPF ?></td>
					      	<td><?php echo $RG ?></td>
					      	<td><?php echo $email ?></td>
						</tr> 
					<?php } ?>
				</tbody>
			</table>			
		</div>
		
	</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>